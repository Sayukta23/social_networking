class AddCachedLikesToPosts < ActiveRecord::Migration[5.2]
  def change
    change_table :posts do |t|
      t.integer :caches_votes_total, default: 0
      t.integer :caches_votes_score, default: 0
      t.integer :caches_votes_up, default: 0
      t.integer :caches_votes_down, default: 0
      t.integer :caches_weighted_score, default: 0
      t.integer :caches_weighted_total, default: 0
      t.float :caches_weighted_average, default: 0
    end
    
  end
end
